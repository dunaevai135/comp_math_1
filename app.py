from flask import Flask, render_template, request
from comp_math.matrix import parse_matrix_from_str, matrix_to_latex, solve

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return render_template("lw1.html")


@app.route('/', methods=['POST'])
def lab1_post():
    matr = request.form['matrix']
    eps = float(request.form['eps'])  # TODO type check

    matrix = parse_matrix_from_str(matr)
    if type(matrix) is str:
        return matrix
    result = solve(matrix, eps)
    if type(result) is str:
        return result

    return render_template('lw1.html', output=result, post_text_matrix=matr)


if __name__ == '__main__':
    app.run()
