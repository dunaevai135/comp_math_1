from _operator import is_
from copy import deepcopy
from functools import reduce
from typing import List, Optional
import itertools

Matrix = List[List[float]]
z_eps = 1e-15
max_iterations = 50000


def parse_matrix_from_str(raw_matrix: str) -> Matrix or str:
    parsed_matrix = []
    for ri, raw_row in enumerate(raw_matrix.split('\n')):
        row = raw_row.split()
        if not row:
            continue

        parsed_row = []

        for ci in range(len(row)):
            try:
                parsed_row.append(float(row[ci]))
            except ValueError:
                return f'Row {ri+1}, col {ci+1} ("{row[ci]}"): not a numeric value.'

        parsed_matrix.append(parsed_row)

    size_error = check_matrix_size_errors(parsed_matrix)
    if size_error:
        return size_error

    return parsed_matrix


def check_matrix_size_errors(matrix: List[List]) -> None or str:
    rows_count = len(matrix)
    cols_count = len(matrix[0]) if rows_count else 0

    if rows_count == 0 or rows_count == 0:
        return "Matrix can't be empty"

    if cols_count > 21 or rows_count > 20:
        return "Matrix is too big"

    for ri, row in enumerate(matrix):
        if cols_count != len(row):
            return f'Row {ri + 1}: expected {cols_count} numbers, got {len(row)} (inconsistent row length)'

    if rows_count != cols_count - 1:
        return f"Matrix is not N x N+1: {rows_count} rows and {cols_count} cols."

    for i in range(len(matrix)):
        if abs(matrix[i][i]) < z_eps:
            return 'Row ' + str(i) + ', col ' + str(i) + ': zero value. Not allowed'

    return None


def matrix_to_latex(matrix: List[List]) -> str:
    def render_float(f: float) -> str:
        s = str(round(f, 3))
        # s = "%.3f" % f
        if 'e' in s:
            return s.replace('e', '* 10^{') + '}'
        else:
            return s

    contents = "\\\\".join(" & ".join(render_float(elem) for elem in row) for row in matrix)
    return f"""
          \\begin{{bmatrix}}
          {contents}
          \\end{{bmatrix}}
    """


class Result:
    def __init__(self,
                 eps: float,
                 determinant: float,
                 dominant_matrix: Matrix,
                 initial_matrix: Matrix,
                 beta_vector: List[float],
                 alpha_matrix: Matrix,
                 diagonally_dominant: bool,
                 iteration_number: int,
                 solution_vector: Optional[List[float]] = None,
                 error_vector: Optional[List[float]] = None):
        self.eps = eps
        self.determinant = determinant
        self.dominant_matrix = dominant_matrix
        self.initial_matrix = initial_matrix
        self.beta_vector = beta_vector
        self.alpha_matrix = alpha_matrix
        self.diagonally_dominant = diagonally_dominant
        self.iteration_number = iteration_number
        self.solution_vector = solution_vector
        self.error_vector = error_vector
        self.has_inf_solutions = abs(determinant) < z_eps

    @property
    def has_no_solutions(self) -> bool:
        return self.solution_vector is None

    @property
    def dominant_matrix_latex(self) -> str:
        return matrix_to_latex(self.dominant_matrix)

    @property
    def initial_matrix_latex(self) -> str:
        return matrix_to_latex(self.initial_matrix)

    @property
    def alpha_matrix_latex(self) -> str:
        return matrix_to_latex(self.alpha_matrix)

    @property
    def beta_vector_latex(self) -> str:
        return matrix_to_latex([[e] for e in self.beta_vector])

    @property
    def solution_vector_latex(self) -> str:
        return matrix_to_latex([[e] for e in self.solution_vector])

    @property
    def error_vector_latex(self) -> str:
        return matrix_to_latex([[e / self.eps] for e in self.error_vector])


def solve(initial_matrix: Matrix, eps: float) -> Result or str:
    size_error = check_matrix_size_errors(initial_matrix)
    if size_error:
        return "Bad matrix"
    n = len(initial_matrix)
    matrix = deepcopy(initial_matrix)  # in order to leave initial_matrix unmodified
    determinant = reduce(lambda a, b: a * b, [matrix[i][i] for i in range(n)])

    for i in range(len(matrix)):
        if abs(matrix[i][i]) < z_eps:
            return "Bad matrix"

    original_diagonally_domination = is_diagonally_dominant(matrix)
    # diagonally_domination = original_diagonally_domination
    dominant_matrix = deepcopy(matrix)
    dominant_matrix = make_diagonally_dominant(dominant_matrix)
    diagonally_domination = True
    if dominant_matrix is None:
        diagonally_domination = False
        dominant_matrix = matrix

    a_matrix = [[0.0 for i in range(n)] for i in range(n)]
    b_vector = [0.0 for i in range(n)]
    for i in range(n):
        for j in range(n):
            if i == j:
                a_matrix[i][j] = 0
            else:
                a_matrix[i][j] = -dominant_matrix[i][j] / dominant_matrix[i][i]
        b_vector[i] = dominant_matrix[i][-1] / dominant_matrix[i][i]

    if not diagonally_domination:
        return Result(eps, determinant, dominant_matrix, initial_matrix, b_vector, a_matrix,
                      original_diagonally_domination, 0, [], [])

    solve_vector = [dominant_matrix[i][-1] for i in range(n)]
    e = 1000
    iterations = 0
    old_solve_vector = solve_vector
    while e > eps and iterations < max_iterations:
        old_solve_vector = solve_vector
        solve_vector = sum_vector(mul_matrix_vector(a_matrix, solve_vector), b_vector)
        e = max(abs(solve_vector[j] - old_solve_vector[j]) for j in range(n))
        iterations += 1

    return Result(eps, determinant, dominant_matrix, initial_matrix, b_vector, a_matrix,
                  diagonally_domination, iterations, solve_vector,
                  [abs(solve_vector[j] - old_solve_vector[j]) for j in range(n)])


def make_diagonally_dominant(matrix: Matrix) -> Matrix or None:
    if is_diagonally_dominant(matrix):
        return matrix
    n = len(matrix)
    array_max = []
    for i in range(n):
        m = max(abs(matrix[i][j]) for j in range(n))
        mi = [abs(matrix[i][j]) for j in range(n)].index(m)
        if m > sum(abs(matrix[i][j]) for j in range(n)) - m:
            if mi not in array_max:
                array_max.append(mi)
    if len(array_max) != n:
        return None
    else:
        a_out = []
        for i in range(n):
            a_out.append(matrix[array_max[i]])
        return a_out


def is_diagonally_dominant(matrix: Matrix) -> bool:
    flag = 0
    n = len(matrix)
    for i in range(len(matrix)):
        if abs(matrix[i][i]) < sum(abs(matrix[i][j]) for j in range(n)) - abs(matrix[i][i]):
            return False
        if abs(matrix[i][i]) > sum(abs(matrix[i][j]) for j in range(n)) - abs(matrix[i][i]):
            flag = 1
    return flag == 1


def mul_matrix_vector(a: Matrix, b: List[float]) -> List[float]:
    c = []
    for i in range(len(a)):
        tmp = 0
        for j in range(len(b)):
            tmp += a[i][j] * b[j]
        c.append(tmp)
    return c


def sum_vector(a: List[float], b: List[float]) -> List[float]:
    c = []
    for i in range(len(a)):
        c.append(a[i] + b[i])
    return c
